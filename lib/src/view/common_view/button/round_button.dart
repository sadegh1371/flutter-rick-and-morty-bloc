import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rick_morty/res/res.dart' as res;

class RoundButton extends StatelessWidget {
  Widget child;
  Function onTap;
  late Color rippleColor;
  late Color backColor;
  RoundedRectangleBorder borderRadius;

  RoundButton(
      {Key? key,
      required this.child,
      required this.onTap,
      required this.borderRadius,
      Color? rippleColor,
      Color? backColor})
      : super(key: key) {
    this.rippleColor = rippleColor ?? res.Colors.colorPrimary;
    this.backColor = backColor ?? res.Colors.colorTrans;
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.all<Size>(
                Size(res.Dimens.tenSize * 3, res.Dimens.tenSize * 2.4)),
            maximumSize: MaterialStateProperty.all<Size>(
                Size(res.Dimens.tenSize * 1000, res.Dimens.tenSize * 4.4)),
            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.only(
              left: res.Dimens.tenSize / 2,
              right: res.Dimens.tenSize / 2,
            )),
            overlayColor: MaterialStateProperty.all<Color>(rippleColor),
            backgroundColor: MaterialStateProperty.all<Color>(backColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(borderRadius)),
        onPressed: () {
          onTap.call();
        },
        child: child);
  }
}
