import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rick_morty/res/res.dart' as res;

class ProgressButton extends StatefulWidget {
  Widget child;
  Widget progress;
  Function onTap;
  late Color rippleColor;
  late Color backColor;
  RoundedRectangleBorder borderRadius;

  bool showProgress = false;

  ProgressButton(
      {Key? key,
      required this.child,
      required this.progress,
      required this.onTap,
      required this.borderRadius,
      this.showProgress = false,
      Color? rippleColor,
      Color? backColor})
      : super(key: key) {
    this.rippleColor = rippleColor ?? res.Colors.colorPrimary;
    this.backColor = backColor ?? res.Colors.colorTrans;
  }

  late _ProgressButtonState state;

  @override
  State<StatefulWidget> createState() {
    state = _ProgressButtonState();
    return state;
  }

  void reload(bool showProgress) {
    if (state.mounted) {
      state.setState(() {
        state.showProgress = showProgress;
      });
    }
  }
}

class _ProgressButtonState extends State<ProgressButton> {
  bool showProgress = false;

  @override
  void initState() {
    showProgress = widget.showProgress;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.all<Size>(
                Size(res.Dimens.tenSize * 3, res.Dimens.tenSize * 2)),
            maximumSize: MaterialStateProperty.all<Size>(
                Size(res.Dimens.tenSize * 1000, res.Dimens.tenSize * 4.4)),
            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.only(
              left: res.Dimens.tenSize / 2,
              right: res.Dimens.tenSize / 2,
            )),
            overlayColor: MaterialStateProperty.all<Color>(widget.rippleColor),
            backgroundColor: MaterialStateProperty.all<Color>(widget.backColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(widget.borderRadius)),
        onPressed: () {
          if (!showProgress) {
            widget.onTap.call();
          }
        },
        child: Container(
          alignment: Alignment.center,
          child: Stack(
            children: [
              AnimatedOpacity(
                opacity: showProgress ? 0.0 : 1.0,
                duration: const Duration(milliseconds: 500),
                child: widget.child,
              ),
              AnimatedOpacity(
                opacity: showProgress ? 1.0 : 0.0,
                duration: const Duration(milliseconds: 500),
                child: widget.progress,
              )
            ],
          ),
        ));
  }
}
