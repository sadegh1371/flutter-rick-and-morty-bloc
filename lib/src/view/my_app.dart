import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rick_morty/res/res.dart' as res;
import 'package:rick_morty/src/libs/ht/html.dart';

import 'splash_screen.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: res.Texts.appName,
        theme: ThemeData(
          fontFamily: 'Roboto',
          primaryColor: res.Colors.colorPrimary,
          textSelectionTheme:
              const TextSelectionThemeData(selectionHandleColor: res.Colors.colorPrimaryLite),
        ),
        home: const SplashScreen(
          seconds: 3,
        ),
        supportedLocales: const [Locale("en", "")],
        locale: const Locale("en", ""));
  }

  @override
  void initState() {
    HtmlFormater.format();
    super.initState();
  }

  Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  double screenHeight(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).height / dividedBy;
  }

  double screenWidth(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).width / dividedBy;
  }
}
