library splashscreen;

import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rick_morty/base/base_stateful_state.dart';
import 'package:rick_morty/res/res.dart' as res;
import 'package:rick_morty/src/libs/ht/html.dart';
import 'package:rick_morty/src/view/main/main_screen.dart';

class SplashScreen extends StatefulWidget {
  final int seconds;

  const SplashScreen({Key? key, required this.seconds}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends BaseStatefulState<SplashScreen> {
  @override
  void initState() {
    HtmlFormater.format();
    super.initState();
    Timer(Duration(seconds: widget.seconds), () {
      goToPageAction();
    });
  }

  @override
  Widget build(BuildContext context) {
    setDimen();
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: const SystemUiOverlayStyle(
            statusBarIconBrightness: Brightness.light,
            systemNavigationBarColor: res.Colors.pageBackground,
            systemNavigationBarIconBrightness: Brightness.light,
            statusBarColor: Colors.transparent),
        child: Scaffold(
          key: scaffoldKey,
          body: InkWell(
            onTap: () {
              goToPageAction();
            },
            child: Container(
              color: res.Colors.pageBackground,
              child: Column(
                children: [
                  Expanded(
                    child: Align(
                      alignment: Alignment.center,
                      child: Image.asset(
                        res.Drawable.icLogo,
                        width: screenWidth(context) * 0.4,
                        height: screenWidth(context) * 0.4,
                      ),
                    ),
                    flex: 9,
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        res.Texts.version,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: res.Dimens.textSizeMid,
                            color: res.Colors.colorWhite),
                      ),
                    ),
                    flex: 1,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  void goToPageAction() async {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (BuildContext context) => const MainScreen()));
  }

  @override
  void hideProgressDialog() {}

  @override
  void showProgressDialog() {}

  @override
  void setPresenter() {}
}
