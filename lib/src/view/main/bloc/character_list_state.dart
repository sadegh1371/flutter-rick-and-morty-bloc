part of 'character_list_bloc.dart';

enum CharacterListStatus { initial, success, failure }

class CharacterListState {
  CharacterListState({
    this.status = CharacterListStatus.initial,
    this.items = const <CharacterItem>[],
    this.word = "",
    this.page = 1,
    this.hasReachedMax = false,
  });

  final CharacterListStatus status;
  final List<CharacterItem> items;
  String word;
  final int page;
  final bool hasReachedMax;

  CharacterListState copyWith({
    CharacterListStatus? status,
    List<CharacterItem>? items,
    String? word,
    int? page,
    bool? hasReachedMax,
  }) {
    return CharacterListState(
      status: status ?? this.status,
      items: items ?? this.items,
      word: word ?? this.word,
      page: page ?? this.page,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  List<Object> get props => [status, items, word, page, hasReachedMax];
}
