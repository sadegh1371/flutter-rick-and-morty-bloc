import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:rick_morty/src/view/main/model/character_item.dart';
import 'package:rick_morty/src/view/main/presenter/character_list_screen_presenter.dart';
import 'package:stream_transform/stream_transform.dart';

part 'character_list_event.dart';
part 'character_list_state.dart';

const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class CharacterListBloc extends Bloc<ItemEvent, CharacterListState> {
  CharacterListBloc() : super(CharacterListState()) {
    on<CharacterListFetched>(
      _onCharacterFetched,
      transformer: throttleDroppable(throttleDuration),
    );
  }

  Future<void> _onCharacterFetched(
    CharacterListFetched event,
    Emitter<CharacterListState> emit,
  ) async {
    if (!event.reload && state.hasReachedMax) return;
    try {
      if (state.status == CharacterListStatus.initial) {
        return emit(await _fetchCharacters(event));
      }
      if (event.reload) {
        emit(state.copyWith(
          status: CharacterListStatus.initial,
          items: [],
          page: 1,
          hasReachedMax: false,
        ));
      }
      emit(await _fetchCharacters(event));
    } catch (_) {
      emit(state.copyWith(status: CharacterListStatus.failure));
    }
  }

  Future<CharacterListState> _fetchCharacters(CharacterListFetched event) async {
    try {
      var response = await event.presenter.getListItems(state.word, state.page);
      if (response.status) {
        var list = json.decode(response.data);
        List<CharacterItem> listMain = [];
        for (int i = 0; i < list.length; i++) {
          listMain.add(CharacterItem.fromJson(list[i]));
        }
        if (listMain.isEmpty) {
          return state.copyWith(hasReachedMax: true);
        } else {
          return state.copyWith(
              status: CharacterListStatus.success,
              items: List.of(state.items)..addAll(listMain),
              page: state.page + 1,
              hasReachedMax: json.decode(response.meta)['pages'] <= state.page + 1);
        }
      } else {
        return state.copyWith(status: CharacterListStatus.failure, hasReachedMax: true);
      }
    } catch (_) {
      return state.copyWith(status: CharacterListStatus.failure, hasReachedMax: true);
    }
  }
}
