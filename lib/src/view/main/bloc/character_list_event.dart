part of 'character_list_bloc.dart';

abstract class ItemEvent {
  @override
  List<Object> get props => [];
}

class CharacterListFetched extends ItemEvent {
  bool reload = false;
  final CharacterListScreenPresenter presenter;
  CharacterListFetched(this.reload, this.presenter);
}
