library homescreen;

import 'dart:async';
import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_morty/base/base_stateful_state.dart';
import 'package:rick_morty/res/res.dart' as res;
import 'package:rick_morty/src/view/main/bloc/character_list_bloc.dart';
import 'package:rick_morty/src/view/main/views/character_item_list_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends BaseStatefulState<MainScreen> {
  @override
  Widget build(BuildContext context) {
    setDimen();
    return Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: res.Colors.actionBarColor,
          title: Text(
            res.Texts.appName,
            style: TextStyle(
                fontSize: res.Dimens.textSizeXLarge,
                fontWeight: FontWeight.bold,
                color: res.Colors.textColor),
          ),
        ),
        body: WillPopScope(
          onWillPop: _onWillPop,
          child: BlocProvider(
            create: (_) => CharacterListBloc(),
            child: const CharacterListItemScreen(),
          ),
        ));
  }

  bool doubleTap = false;

  Future<bool> _onWillPop() async {
    if (doubleTap) {
      return true;
    } else {
      doubleTap = true;
      Timer(const Duration(seconds: 2), () {
        doubleTap = false;
      });
      showError(res.Texts.doubleBack);
      return false;
    }
  }

  @override
  void hideProgressDialog() {}

  @override
  void showProgressDialog() {}

  @override
  void setPresenter() {}
}
