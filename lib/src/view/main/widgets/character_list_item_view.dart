import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rick_morty/res/res.dart' as res;
import 'package:rick_morty/src/view/main/model/character_item.dart';

class CharacterItemView extends StatelessWidget {
  TextStyle titleTextStyle = TextStyle(
      fontSize: res.Dimens.textSizeMid, fontWeight: FontWeight.bold, color: res.Colors.textColor);

  TextStyle descTextStyle = TextStyle(
      fontSize: res.Dimens.textSizeSmall,
      fontWeight: FontWeight.normal,
      color: res.Colors.textColor);

  CharacterItemView({Key? key, required this.item}) : super(key: key);

  final CharacterItem item;

  @override
  Widget build(BuildContext context) {
    var circleColor = res.Colors.unknown;
    switch (item.status.toLowerCase()) {
      case "alive":
        circleColor = res.Colors.green;
        break;
      case "dead":
        circleColor = res.Colors.red;
        break;
    }

    return Container(
      margin: EdgeInsets.only(top: res.Dimens.tenSize * 0.8, bottom: res.Dimens.tenSize * 0.8),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(res.Dimens.tenSize)),
        child: Container(
          width: double.infinity,
          height: res.Dimens.tenSize * 12,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(res.Dimens.tenSize)),
              color: res.Colors.listItemBackground),
          child: Row(
            children: [
              CachedNetworkImage(
                imageUrl: item.img,
                width: res.Dimens.tenSize * 12,
                height: res.Dimens.tenSize * 12,
                placeholder: (a, b) {
                  return Padding(
                    padding: EdgeInsets.all(res.Dimens.tenSize),
                    child: Image.asset(
                      res.Drawable.icLogo,
                      width: res.Dimens.tenSize * 12,
                      height: res.Dimens.tenSize * 12,
                      fit: BoxFit.scaleDown,
                    ),
                  );
                },
                fit: BoxFit.cover,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(res.Dimens.tenSize * 2.1, res.Dimens.tenSize * 1.5,
                      res.Dimens.tenSize * 1.5, res.Dimens.tenSize * 1.5),
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          item.name,
                          style: titleTextStyle,
                          maxLines: 1,
                          overflow: TextOverflow.fade,
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: res.Dimens.tenSize * 0.8,
                            height: res.Dimens.tenSize * 0.8,
                            margin: EdgeInsets.only(right: res.Dimens.tenSize * 0.6),
                            decoration: BoxDecoration(shape: BoxShape.circle, color: circleColor),
                          ),
                          Text(
                            item.status,
                            style: descTextStyle,
                            maxLines: 1,
                            overflow: TextOverflow.fade,
                          )
                        ],
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Text(
                              res.Texts.species,
                              style: descTextStyle.copyWith(color: res.Colors.textColorHint),
                              maxLines: 1,
                              overflow: TextOverflow.fade,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              res.Texts.gender,
                              style: descTextStyle.copyWith(color: res.Colors.textColorHint),
                              maxLines: 1,
                              overflow: TextOverflow.fade,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              child: Text(
                            item.species,
                            style: descTextStyle,
                            maxLines: 1,
                            overflow: TextOverflow.fade,
                          )),
                          Expanded(
                              child: Text(
                            item.gender,
                            style: descTextStyle,
                            maxLines: 1,
                            overflow: TextOverflow.fade,
                          )),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
