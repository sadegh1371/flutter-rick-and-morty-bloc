import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_morty/base/base_stateful_state.dart';
import 'package:rick_morty/res/res.dart' as res;
import 'package:rick_morty/src/view/common_view/progress/in_page_progress.dart';
import 'package:rick_morty/src/view/main/bloc/character_list_bloc.dart';
import 'package:rick_morty/src/view/main/model/character_list_screen_model.dart';
import 'package:rick_morty/src/view/main/presenter/character_list_screen_presenter.dart';
import 'package:rick_morty/src/view/main/widgets/character_list_item_view.dart';

class CharacterListItemScreen extends StatefulWidget {
  const CharacterListItemScreen({Key? key}) : super(key: key);

  @override
  _CharacterListItemScreenState createState() => _CharacterListItemScreenState();
}

class _CharacterListItemScreenState extends BaseStatefulState<CharacterListItemScreen> {
  final TextEditingController _controller = TextEditingController();

  TextStyle titleTextStyle = TextStyle(
      fontSize: res.Dimens.textSizeMid, fontWeight: FontWeight.bold, color: res.Colors.textColor);

  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    context
        .read<CharacterListBloc>()
        .add(CharacterListFetched(true, presenter as CharacterListScreenPresenter));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: res.Colors.pageBackground,
      child: BlocBuilder<CharacterListBloc, CharacterListState>(
        builder: (context, state) {
          Widget widget;
          switch (state.status) {
            case CharacterListStatus.failure:
              widget = state.items.isNotEmpty || _controller.text.isNotEmpty
                  ? _pageContent(state)
                  : _emptyList(res.Texts.dataError, res.Drawable.icError);
              break;
            case CharacterListStatus.success:
              widget = _pageContent(state);
              break;
            default:
              widget = Center(
                  child: Container(
                      margin: EdgeInsets.only(top: res.Dimens.tenSize * 7.1),
                      child: InPageProgress(
                        color: res.Colors.colorWhite,
                      )));
              break;
          }
          return Stack(
            children: [widget, _searchView(state)],
          );
        },
      ),
    );
  }

  Widget _searchView(CharacterListState state) {
    return Container(
      height: res.Dimens.tenSize * 4.7,
      margin: EdgeInsets.fromLTRB(
          res.Dimens.tenSize * 1.6, res.Dimens.tenSize * 2.4, res.Dimens.tenSize * 1.6, 0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(res.Dimens.tenSize)),
          color: res.Colors.colorWhite),
      child: CupertinoTextField(
        controller: _controller,
        textDirection: TextDirection.ltr,
        textAlign: TextAlign.left,
        style: TextStyle(
          color: res.Colors.pageBackground,
          fontSize: res.Dimens.textSizeLarge,
          fontWeight: FontWeight.normal,
        ),
        cursorColor: res.Colors.colorPrimary,
        maxLines: 1,
        placeholder: res.Texts.searchHint,
        placeholderStyle: TextStyle(
            fontSize: res.Dimens.textSizeLarge,
            fontWeight: FontWeight.normal,
            color: res.Colors.searchHintTextColor),
        decoration: const BoxDecoration(color: Colors.transparent),
        padding: EdgeInsets.zero,
        textInputAction: TextInputAction.search,
        onSubmitted: (value) {
          state.word = _controller.text;
          context
              .read<CharacterListBloc>()
              .add(CharacterListFetched(true, presenter as CharacterListScreenPresenter));
        },
        prefix: Container(
            padding: EdgeInsets.all(res.Dimens.tenSize * 1.2),
            margin: EdgeInsets.only(left: res.Dimens.tenSize * 0.0),
            child: Image.asset(
              res.Drawable.icSearch,
              width: res.Dimens.tenSize * 2.4,
              height: res.Dimens.tenSize * 2.4,
              color: res.Colors.searchHintTextColor,
            )),
        suffixMode: OverlayVisibilityMode.editing,
        suffix: Container(
            padding: EdgeInsets.all(res.Dimens.tenSize * 1.2),
            margin: EdgeInsets.only(left: res.Dimens.tenSize * 0.0),
            child: InkWell(
              child: Image.asset(
                res.Drawable.icCircleClose,
                width: res.Dimens.tenSize * 2.4,
                height: res.Dimens.tenSize * 2.4,
                color: res.Colors.searchHintTextColor,
              ),
              onTap: () {
                _controller.text = "";
                state.word = _controller.text;
                context
                    .read<CharacterListBloc>()
                    .add(CharacterListFetched(true, presenter as CharacterListScreenPresenter));
              },
            )),
      ),
    );
  }

  Widget _emptyList(String text, String image) {
    return InkWell(
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: res.Dimens.tenSize * 14,
            height: res.Dimens.tenSize * 16,
            child: Image.asset(
              image,
              fit: BoxFit.fitHeight,
            ),
          ),
          SizedBox(
            height: res.Dimens.tenSize * 2,
          ),
          Text(
            text,
            style: titleTextStyle,
          )
        ],
      )),
      onTap: () {
        context
            .read<CharacterListBloc>()
            .add(CharacterListFetched(true, presenter as CharacterListScreenPresenter));
      },
    );
  }

  Widget _pageContent(CharacterListState state) {
    if (state.items.isEmpty) {
      return _emptyList(res.Texts.nothingFound, res.Drawable.icNotFound);
    }
    return Container(
      color: res.Colors.pageBackground,
      width: screenWidth(context),
      height: double.infinity,
      padding: EdgeInsets.only(
          left: res.Dimens.tenSize * 1.6,
          right: res.Dimens.tenSize * 1.6,
          top: res.Dimens.tenSize * 6.5),
      child: ScrollConfiguration(
        behavior: const ScrollBehavior(),
        child: GlowingOverscrollIndicator(
          axisDirection: AxisDirection.down,
          color: res.Colors.pageBackground,
          child: ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return index >= state.items.length
                  ? SizedBox(
                      height: res.Dimens.tenSize * 4,
                      child: InPageProgress(
                        color: res.Colors.colorWhite,
                      ),
                    )
                  : Column(
                      children: [
                        SizedBox(
                          height: index == 0 ? res.Dimens.tenSize * 2.2 : 0,
                        ),
                        CharacterItemView(item: state.items[index])
                      ],
                    );
            },
            itemCount: state.hasReachedMax ? state.items.length : state.items.length + 1,
            controller: _scrollController,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) {
      context
          .read<CharacterListBloc>()
          .add(CharacterListFetched(false, presenter as CharacterListScreenPresenter));
    }
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }

  @override
  void hideProgressDialog() {}

  @override
  void showProgressDialog() {}

  @override
  void setPresenter() {
    presenter = CharacterListScreenPresenter(this, CharacterListScreenModel());
  }
}
