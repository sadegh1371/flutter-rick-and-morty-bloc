import 'package:rick_morty/base/base_presenter.dart';
import 'package:rick_morty/src/view/main/model/character_list_screen_model.dart';
import 'package:rick_morty/src/webservice/service_response_model.dart';
import 'package:rick_morty/src/webservice/webservice_middleware.dart';

class CharacterListScreenPresenter extends BasePresenter {
  CharacterListScreenPresenter(screen, CharacterListScreenModel baseModel)
      : super(screen, baseModel);

  Future<ServiceResponseModel> getListItems(String word, int page) async {
    WebserviceMiddleware middleware = WebserviceMiddleware(context, screen, true);

    var serviceResponseModel = await middleware.run(() async {
      return await (model as CharacterListScreenModel).getListItems(word, page);
    });

    return Future.value(serviceResponseModel);
  }
}
