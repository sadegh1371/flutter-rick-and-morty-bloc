import 'dart:collection';

class CharacterItem {
  int id;
  String name, img, status, species, gender;

  CharacterItem(this.id, this.name, this.img, this.status, this.species, this.gender);

  factory CharacterItem.fromJson(LinkedHashMap<String, dynamic> json) {
    int id = 0;
    String name = "", img = "", status = "", species = "", gender = "";

    if (json.containsKey('id')) id = json['id'];
    if (json.containsKey('name')) name = json['name'];
    if (json.containsKey('status')) status = json['status'];
    if (json.containsKey('species')) species = json['species'];
    if (json.containsKey('gender')) gender = json['gender'];
    if (json.containsKey('image')) img = json['image'];

    return CharacterItem(id, name, img, status, species, gender);
  }
}
