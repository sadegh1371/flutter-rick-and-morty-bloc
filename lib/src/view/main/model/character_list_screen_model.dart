import 'package:rick_morty/base/base_model.dart';
import 'package:rick_morty/src/webservice/service_response_model.dart';
import 'package:rick_morty/src/webservice/webservice.dart';

class CharacterListScreenModel extends BaseModel {
  Future<ServiceResponseModel> getListItems(String word, int page) async {
    ServiceResponseModel result = await WebService().searchInPage(word, page);
    return result;
  }
}
