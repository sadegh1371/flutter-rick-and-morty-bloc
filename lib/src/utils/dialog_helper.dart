import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rick_morty/res/res.dart' as res;

mixin DialogHelper {
  static Future showBottomSheetDialog(
    BuildContext context,
    String title,
    Widget view, {
    Color titleColor = res.Colors.colorPrimary,
    bool dismissible = true,
    bool enableDrag = true,
    bool enableBack = true,
  }) {
    return showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isDismissible: dismissible,
        enableDrag: enableDrag,
        barrierColor: res.Colors.colorBarrier,
        builder: (builder) {
          return BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
            child: WillPopScope(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(child: Container()),
                  (title.isEmpty
                      ? Container()
                      : Container(
                          padding:
                              EdgeInsets.only(left: res.Dimens.tenSize, right: res.Dimens.tenSize),
                          decoration: BoxDecoration(
                              color: res.Colors.actionBarColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(res.Dimens.tenSize),
                                  topRight: Radius.circular(res.Dimens.tenSize))),
                          height: res.Dimens.tenSize * 4.2,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                width: res.Dimens.tenSize * 3,
                                height: res.Dimens.tenSize * 3,
                              ),
                              Text(title,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: res.Dimens.textSizeSmall,
                                      color: titleColor)),
                              InkWell(
                                child: Container(
                                  width: res.Dimens.tenSize * 2.6,
                                  height: res.Dimens.tenSize * 2.6,
                                  alignment: Alignment.center,
                                  child: Image.asset(
                                    res.Drawable.icCircleClose,
                                    width: res.Dimens.tenSize * 1.8,
                                    height: res.Dimens.tenSize * 1.8,
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).maybePop();
                                },
                              ),
                            ],
                          ),
                        )),
                  view
                ],
              ),
              onWillPop: () async => enableBack,
            ),
          );
        });
  }

  static void showFullScreenBottomSheetDialog(
    BuildContext context,
    Widget view, {
    bool dismissible = true,
    bool enableDrag = true,
    bool enableBack = true,
  }) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: res.Colors.colorBarrier,
        isScrollControlled: true,
        isDismissible: dismissible,
        enableDrag: enableDrag,
        builder: (builder) {
          return BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
            child: WillPopScope(
              child: view,
              onWillPop: () async => enableBack,
            ),
          );
        });
  }
}
