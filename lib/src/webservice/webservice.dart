import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:rick_morty/res/res.dart' as res;
import 'package:rick_morty/src/webservice/service_response_model.dart';

class WebService {
  static const String _baseUrl = "https://rickandmortyapi.com/api";
  final int _timeOut = 15 * 1000;

  static late Dio _dio;

  static final WebService _singleton = WebService._internal();

  factory WebService() {
    return _singleton;
  }

  WebService._internal() {
    _dio = Dio(BaseOptions(
        connectTimeout: _timeOut,
        receiveTimeout: _timeOut,
        sendTimeout: _timeOut * 2,
        headers: {
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.contentTypeHeader: "application/json",
        }));
  }

  Future<ServiceResponseModel> searchInPage(String word, int page) {
    String url = _baseUrl + "/character?page=$page" + (word.isNotEmpty ? "&name=$word" : "");
    return _callApi(MethodType.GET, url);
  }

  Future<ServiceResponseModel> _callApi(MethodType type, String callUrl, {Map? body}) async {
    try {
      Response response;
      switch (type) {
        case MethodType.GET:
          response = await _dio.get(callUrl);
          break;
        case MethodType.POST:
          response = await _dio.post(callUrl, data: json.encode(body));
          break;
        case MethodType.DELETE:
          response = await _dio.delete(callUrl);
          break;
        case MethodType.PUT:
          response = await _dio.put(callUrl);
          break;
      }

      if (response.statusCode! < 300 && response.statusCode! >= 200) {
        return ServiceResponseModel(true, "", json.encode(response.data['results']),
            meta: json.encode(response.data['info']));
      } else {
        if (response.statusCode == 408) {
          return ServiceResponseModel(false, res.Texts.noConnection, "");
        } else {
          if (response.data != null && response.data.contains("desc")) {
            return ServiceResponseModel(false, jsonDecode(response.data)["desc"].toString(), "");
          } else {
            return ServiceResponseModel(false, res.Texts.noConnection, "");
          }
        }
      }
    } on DioError catch (ex) {
      if (ex.response != null) {
        return ServiceResponseModel(false, ex.response!.data, "");
      } else {
        return ServiceResponseModel(false, ex.message, "");
      }
    }
  }

  static Future<bool> hasConnection() async {
    if (kIsWeb) return true;
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
}

enum MethodType { GET, POST, DELETE, PUT }
