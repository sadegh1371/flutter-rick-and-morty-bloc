import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:rick_morty/res/res.dart' as res;
import 'package:rick_morty/src/utils/dialog_helper.dart';

import 'service_response_model.dart';
import 'webservice.dart';
import 'webservice_progress_callback.dart';

class WebserviceMiddleware {
  bool retryEnable;
  Function? onCancel;
  BuildContext context;
  WebserviceProgressCallback progressCallback;

  bool showProgress = false;

  WebserviceMiddleware(this.context, this.progressCallback, this.retryEnable,
      {this.showProgress = false, this.onCancel});

  Future<ServiceResponseModel> run(Function callingFunction, {bool dismissAfter = true}) async {
    try {
      bool connected = true;
      connected = await WebService.hasConnection();

      if (connected) {
        if (showProgress) progressCallback.showProgressDialog();
        ServiceResponseModel result = await callingFunction.call();
        if (dismissAfter) progressCallback.hideProgressDialog();
        if (result.status) {
          return result;
        } else {
          if (result.message == res.Texts.noConnection && retryEnable) {
            bool tryAgain = await showTryAgainDialog();
            if (tryAgain) {
              return run(callingFunction);
            } else {
              onCancel!.call();
            }
          } else {
            return result;
          }
        }
      } else {
        bool tryAgain = await showTryAgainDialog();
        if (tryAgain) {
          return run(callingFunction);
        } else {
          onCancel!.call();
        }
      }
    } catch (ex) {
      if (kDebugMode) {
        print(ex);
      }
    }

    if (dismissAfter) progressCallback.hideProgressDialog();
    return ServiceResponseModel(false, res.Texts.noConnection, "");
  }

  Future showTryAgainDialog() {
    Widget cancelButton = TextButton(
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.all<Size>(
                Size(res.Dimens.tenSize * 3, res.Dimens.tenSize * 3.8)),
            maximumSize: MaterialStateProperty.all<Size>(
                Size(res.Dimens.tenSize * 1000, res.Dimens.tenSize * 3.8)),
            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.only(
              left: res.Dimens.tenSize / 2,
              right: res.Dimens.tenSize / 2,
            )),
            overlayColor: MaterialStateProperty.all<Color>(res.Colors.redDark),
            backgroundColor: MaterialStateProperty.all<Color>(res.Colors.red),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(res.Dimens.roundBtnRadios),
                side: const BorderSide(width: 1, color: res.Colors.red)))),
        onPressed: () {
          Navigator.pop(context, false);
        },
        child: Container(
          width: res.Dimens.tenSize * 40 * 0.9,
          alignment: Alignment.center,
          child: Text(res.Texts.cancel,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: res.Dimens.textSizeSmall,
                  fontWeight: FontWeight.bold)),
        ));

    Widget continueButton = TextButton(
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.all<Size>(
                Size(res.Dimens.tenSize * 3, res.Dimens.tenSize * 3.8)),
            maximumSize: MaterialStateProperty.all<Size>(
                Size(res.Dimens.tenSize * 1000, res.Dimens.tenSize * 3.8)),
            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.only(
              left: res.Dimens.tenSize / 2,
              right: res.Dimens.tenSize / 2,
            )),
            overlayColor: MaterialStateProperty.all<Color>(res.Colors.colorPrimaryDark),
            backgroundColor: MaterialStateProperty.all<Color>(res.Colors.colorPrimary),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(res.Dimens.roundBtnRadios),
                side: const BorderSide(width: 1, color: res.Colors.colorPrimary)))),
        onPressed: () {
          Navigator.pop(context, true);
        },
        child: Container(
          width: res.Dimens.tenSize * 40 * 0.9,
          alignment: Alignment.center,
          child: Text(res.Texts.tryAgain,
              style: TextStyle(
                  color: res.Colors.colorWhite,
                  fontSize: res.Dimens.textSizeSmall,
                  fontWeight: FontWeight.bold)),
        ));

    var widget = Container(
      height: res.Dimens.tenSize * 15,
      padding: EdgeInsets.only(
          left: res.Dimens.tenSize, top: res.Dimens.tenSize, right: res.Dimens.tenSize),
      color: res.Colors.pageBackground,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            res.Texts.noNetDesc,
            style: TextStyle(
                color: res.Colors.textColor,
                fontSize: res.Dimens.textSizeSmall,
                fontWeight: FontWeight.normal),
          ),
          Row(
            children: [
              Expanded(child: continueButton),
              SizedBox(
                width: res.Dimens.tenSize,
              ),
              Expanded(child: cancelButton),
            ],
          )
        ],
      ),
    );

    return DialogHelper.showBottomSheetDialog(context, res.Texts.noNetTitle, widget,
        titleColor: res.Colors.colorWhite);
  }
}
