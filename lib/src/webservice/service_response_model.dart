class ServiceResponseModel {
  bool status;
  String message;
  dynamic data;
  dynamic meta;

  ServiceResponseModel(this.status, this.message, this.data, {this.meta});

  factory ServiceResponseModel.fromJson(Map<String, dynamic> json) {
    String msg = "";
    dynamic data;
    dynamic meta;
    bool status = true;
    if (json.containsKey('status')) status = json['status'];
    if (json.containsKey('msg')) msg = json['msg'];
    if (json.containsKey('data')) data = json['data'];
    if (json.containsKey('meta')) meta = json['meta'];

    return ServiceResponseModel(status, msg, data, meta: meta);
  }
}
