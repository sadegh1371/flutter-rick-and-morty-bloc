import 'dart:ui';

import 'package:flutter/material.dart';

class Drawable {
  static const String icLogo = "assets/images/app_logo.png";
  static const String icCircleClose = "assets/images/circle_close.png";
  static const String icSearch = "assets/images/ic_search.png";
  static const String icError = "assets/images/ic_error.png";
  static const String icNotFound = "assets/images/ic_not_found.png";
}

class Colors {
  static const Color colorPrimary = Color(0xFF3B28CC);
  static const Color colorPrimaryTrans = Color(0xffc4bff0);
  static const Color colorPrimaryLite = Color(0xFF5344CD);
  static const Color colorPrimaryDark = Color(0xFF2A1D8E);
  static const Color colorAccent = Color(0xFF6eb9bc);
  static const Color colorDim = Color(0x88555555);
  static const Color colorAccentTrans = Color(0x306eb9bc);

  static const Color colorTrans = Color(0x00000000);

  static const Color colorBarrier = Color(0x77555555);
  static const Color pageBackground = Color(0xff21232A);
  static const Color actionBarColor = Color(0xff141414);
  static const Color textColor = Color(0xffffffff);
  static const Color textColorHint = Color(0xff828283);
  static const Color searchHintTextColor = Color(0xff757575);
  static const Color colorWhite = Color(0xffffffff);

  static const Color listItemBackground = Color(0xff35363C);

  static const Color redDark = Color(0xFF951C1F);
  static const Color red = Color(0xFFC82B2F);
  static const Color green = Color(0xFF50CD30);
  static const Color unknown = Color(0xFFDCDCDC);
}

class Dimens {
  static double textSizeXLarge = 18.0;
  static double textSizeLarge = 16.0;
  static double textSizeMid = 14.0;
  static double textSizeSmall = 12.0;

  static double roundBtnRadios = 12.0;

  static double tenSize = 10.0;

  static double actionBarSize = 10.0;
  static const double roundBtnLarge = 50.0;
}

class Constants {
  static const int snackBarDuration = 4;
}

class Texts {
  static const String appName = "Rick And Morty";
  static const String version = "Ver 1.0.0";
  static const String doubleBack = "Press back for exit.";

  static const String noConnection = "No Internet Connection!";
  static const String tryAgain = "Try Again";
  static const String cancel = "Cancel";

  static const String species = "Species:";
  static const String gender = "Gender:";

  static const String dataError = "Failed to get data, try again later.";
  static const String nothingFound = "Nothing found.";

  static const String searchHint = "Search Name";

  static const String noNetTitle = "Internet Connection";
  static const String noNetDesc = "Internet access is not possible. Please check your device "
      "internet. ";
}
