import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:rick_morty/base/base_iface.dart';
import 'package:rick_morty/base/base_presenter.dart';
import 'package:rick_morty/res/res.dart' as res;
import 'package:rick_morty/src/libs/ht/html.dart';

abstract class BaseStatefulState<T extends StatefulWidget> extends State<T> implements BaseIFace {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  BasePresenter? presenter;

  @override
  void initState() {
    HtmlFormater.format();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    super.initState();
    setPresenter();
    if (presenter != null) presenter!.context = context;
  }

  void setPresenter();

  Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  double screenHeight(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).height / dividedBy;
  }

  double screenWidth(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).width / dividedBy;
  }

  @override
  void showSnack(String text) {
    if (text.isEmpty || scaffoldKey.currentState == null) return;
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      backgroundColor: const Color(0xff57595F),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        topLeft: Radius.circular(res.Dimens.tenSize),
        topRight: Radius.circular(res.Dimens.tenSize),
      )),
      content: Text(
        text,
        softWrap: true,
        style: TextStyle(fontSize: res.Dimens.textSizeMid, color: res.Colors.textColor),
      ),
    ));
  }

  @override
  void showError(String text) {
    if (text.isEmpty || scaffoldKey.currentState == null) return;
    showSnack(text);
  }

  void onDismiss() {}

  bool is16To9() {
    return screenWidth(context) / screenHeight(context) >= 9 / 19;
  }

  void setDimen() {
    res.Dimens.tenSize = screenWidth(context, dividedBy: 40);

    res.Dimens.actionBarSize = MediaQuery.of(context).padding.top;

    res.Dimens.textSizeLarge = res.Dimens.tenSize * 1.8;
    res.Dimens.textSizeXLarge = res.Dimens.textSizeLarge * 1.12;
    res.Dimens.textSizeMid = res.Dimens.textSizeLarge * 0.88;
    res.Dimens.textSizeSmall = res.Dimens.textSizeMid * 0.88;

    res.Dimens.roundBtnRadios = res.Dimens.tenSize * 1.2;
  }
}
