import 'package:flutter/material.dart';
import 'package:rick_morty/base/base_model.dart';

class BasePresenter<Y> {
  BaseModel model;
  Y screen;

  late BuildContext context;

  set buildContext(BuildContext value) {
    context = value;
  }

  BasePresenter(this.screen, this.model);
}
