import 'package:rick_morty/src/webservice/webservice_progress_callback.dart';

class BaseIFace implements WebserviceProgressCallback {
  @override
  void hideProgressDialog() {}

  @override
  void showProgressDialog() {}

  @override
  void showSnack(String text) {}

  @override
  void showError(String text) {}
}
